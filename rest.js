var url = require('url');
var http = require('http');
var fs = require('fs');
var ROOT_DIR = process.cwd()+'/html' ;
http.createServer(function (req, res) {
  var urlObj = url.parse(req.url, true, false);
  console.log("URL path " + urlObj.pathname);
  console.log("URL search " + urlObj.search);
  console.log("URL query " + urlObj.query["q"]);

  if (urlObj.pathname.indexOf("getcity") !=-1) {
    console.log("In REST Service");
    fs.readFile('cities.dat.txt', function(err,data) {
    	if (err) throw err;
    	var jsonResult = [];
    	var myRe = new RegExp("^"+urlObj.query['q']);
    	console.log(myRe);
    	cities = data.toString().split("\n");
    	for (var i = 0; i < cities.length; i++) {
    		var result = cities[i].search(myRe);
    		if (result != -1) {
    			console.log(cities[i]);
    			jsonResult.push({city:cities[i]});
    		}
    	}
    	console.log(jsonResult);
    	res.writeHead(200);
			res.end(JSON.stringify(jsonResult));
    });
  } else if (urlObj.pathname.indexOf("comment") !=-1) {
        console.log("Comments");
        if (req.method === "POST") {
          console.log("POSTED");
          var jsonData = '';
          req.on('data', function(chunk) {
            jsonData += chunk;
          });
          req.on('end', function(){
            console.log(jsonData);
            var reqObj = JSON.parse(jsonData);
            console.log(reqObj);
            console.log("Name: "+reqObj.Name);
            console.log("Comment: "+reqObj.Comment);
            var MongoClient = require('mongodb').MongoClient;
            MongoClient.connect("mongodb://localhost/comment", function(err, db) {
              if(err) throw err;
              db.collection('comments').insert(reqObj,function(err, records) {
                console.log("Record added as "+records[0]._id);
                res.writeHead(204);
                res.end();
              });
            });
          });
        } else {
          var MongoClient = require('mongodb').MongoClient;
          MongoClient.connect("mongodb://localhost/comment", function(err, db) {
            if(err) throw err;
            db.collection("comments", function(err, comments){
              if(err) throw err;
              comments.find(function(err, items){
                items.toArray(function(err, itemArr){
                  console.log("Document Array: ");
                  console.log(itemArr);
                  res.writeHead(200);
                  res.end(JSON.stringify(itemArr));
                });
              });
            });
          });
        }
  } else {
	  fs.readFile(ROOT_DIR + urlObj.pathname, function (err,data) {
	    if (err) {
	      res.writeHead(404);
	      res.end(JSON.stringify(err));
	      return;
	    }
      res.writeHead(200);
      res.end(data);

  	});
	}
}).listen(80);
