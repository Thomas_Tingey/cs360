var express = require('express');
var https = require('https');
var http = require('http');
var fs = require('fs');
var url = require('url');
var bodyParser = require('body-parser');
var basicAuth = require('basic-auth-connect');
var MongoClient = require('mongodb').MongoClient;
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ 
	extended: true 
}));

var auth = basicAuth( function(user, pass) { 
	return ((user ==='cs360') && (pass === 'test')); 
});

var options = {
	host: '127.0.0.1',
	key: fs.readFileSync('ssl/server.key'),
	cert: fs.readFileSync('ssl/server.crt')
};

http.createServer(app).listen(80);
https.createServer(options, app).listen(443);
app.get('/', function (req, res) {
	console.log("Get instance");
	res.send("Get Index");
});

app.use('/', express.static('./html', {maxAge: 60*60*1000}));
app.use('/img', express.static( './html/img')).
	use('/lib', express.static( './html/img'));

app.get('/getcity', function (req, res) {
	console.log("In REST Service");
	var urlObj = url.parse(req.url, true, false);
	fs.readFile('cities.dat.txt', function(err,data) {
		if (err) throw err;
		var jsonResult = [];
		var myRe = new RegExp("^"+urlObj.query['q']);
		console.log(myRe);
		cities = data.toString().split("\n");
		for (var i = 0; i < cities.length; i++) {
			var result = cities[i].search(myRe);
			if (result != -1) {
				console.log(cities[i]);
				jsonResult.push({city:cities[i]});
			}
		}
		console.log(jsonResult);
		res.writeHead(200);
			res.end(JSON.stringify(jsonResult));
	});
});

app.get('/comment', function (req, res) {
	MongoClient.connect("mongodb://localhost/comment", function(err, db) {
		if(err) throw err;
		db.collection("comments", function(err, comments){
			if(err) throw err;
			comments.find(function(err, items){
				items.toArray(function(err, itemArr) {
					console.log("Document Array: ");
					console.log(itemArr);
					res.writeHead(200);
					res.end(JSON.stringify(itemArr));
				});
			});
		});
	});
});

app.post('/comment', auth, function (req, res) {
	console.log("POSTED");
	MongoClient.connect("mongodb://localhost/comment", function(err, db) {
		if(err) throw err;
		
		// Show authentication console
		req.remoteUser;

		db.collection('comments').insert(req.body,function(err, records) {
			console.log("Record added as "+records[0]._id);
			res.writeHead(200);
			res.end();
		});
	});
});